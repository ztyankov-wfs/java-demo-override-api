package com.workforcesoftware.demooverride;

//------------------------------------------------------------------
// Copyright (c) 1999, 2007
// WorkForce Software, Inc.
// All rights reserved.
//
// Web-site: http://www.workforcesoftware.com
// E-mail:   support@workforcesoftware.com
// Phone:    (877) 493-6723
//
// This program is protected by copyright laws and is considered
// a trade secret of WorkForce Software.  Access to this program
// and source code is granted only to licensed customers.  Under
// no circumstances may this software or source code be distributed
// without the prior written consent of WorkForce Software.
// -----------------------------------------------------------------

import java.time.LocalDate;

public interface DateOverride {
  public LocalDate getSystemDate();
}
