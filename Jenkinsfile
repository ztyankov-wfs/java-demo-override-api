node() {
  currentBuild.description = "Build for Lighthouse Java Demo Override API"

  try {
    stage('Workspace Cleanup') {
      echo 'Cleaning up workspace...'
      deleteDir()
    }

    stage('Checkout code') {
      checkout scm
    }

    //Tell BitBucket a build is in progress
    bitbucketStatusNotify(buildState: 'INPROGRESS')

    def cleanedBranchName = env.BRANCH_NAME.replace('/','_')
    def dockerImage = 'UNKNOWN'

    //Initialize Docker image in a separate stage as it can sometimes take a long time
    stage('Initialize Docker Image') {
      //For details on what is installed in Docket image, check /Dockerimages/build/Dockerimage
      dockerImage = docker.build("java-demo-override-api:${cleanedBranchName}_${env.BUILD_ID}", "-f ./Dockerfiles/build/Dockerfile .")
      withEnv(["CLEANED_BRANCH_NAME=${cleanedBranchName}"]) {
        dockerImage.inside() {
          echo 'Docker initialization complete.'
        }
      }
    }

    withEnv(["CLEANED_BRANCH_NAME=${cleanedBranchName}"]) {
      dockerImage.inside() {

        stage('Build'){
          // run your build
          echo 'Starting build'
          sh 'mv sample_gradle.properties gradle.properties'
          sh './build.sh'
        }

        stage('Test') {
          // run your tests
          echo 'Running tests'
          sh './test.sh'
        }
      }
    }

    dockerImage.inside() {
      withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'artifactory', usernameVariable: 'ARTIFACTORY_USER', passwordVariable: 'ARTIFACTORY_PASSWORD']]) {
        stage('Publish JAR to artifactory') {
          def projectVersion = sh script: "./gradlew properties -q | grep 'version:' | awk '{print \$2}'", returnStdout: true
          echo 'Version:' + projectVersion
          if (env.BRANCH_NAME == 'develop' && projectVersion.trim().endsWith('SNAPSHOT')) {
            echo 'Develop branch: publishing SNAPSHOT to artifactory...'
            sh './gradlew -Partifactory_user=$ARTIFACTORY_USER -Partifactory_password=$ARTIFACTORY_PASSWORD publish'
            echo 'Demo Override API Library SNAPSHOT uploaded to artifactory'
          } else if (env.BRANCH_NAME == 'master' && !projectVersion.trim().endsWith('SNAPSHOT') && (env.GIT_COMMIT != env.GIT_PREVIOUS_SUCCESSFUL_COMMIT)) {
            echo 'Changes detected in master branch - publishing Jar to artifactory'
            echo 'Note this may fail if version number is not properly updated'
            sh './gradlew -Partifactory_user=$ARTIFACTORY_USER -Partifactory_password=$ARTIFACTORY_PASSWORD publish'
            echo 'Demo Override API Library version ' + projectVersion.trim() + ' uploaded to artifactory'
          } else {
            echo 'Skipping publishing to artifactory as not a release branch'
          }
        }
      }
    }

    bitbucketStatusNotify(buildState: 'SUCCESSFUL')
    stage('Successful Build Cleanup') {
      echo 'Cleaning up workspace for successful build...'
      deleteDir()
      echo 'Done.'
    }
  } catch (Exception e) {
      bitbucketStatusNotify(buildState: 'FAILED')
      throw e // Rethrow exception to Jenkins knows its a failed status
  }
}
